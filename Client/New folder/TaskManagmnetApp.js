 angular.module('TaskApp', ['ngRoute'])
	
	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		//$locationProvider.hashPrefix('');
		
		$routeProvider	.when('/', 			{ templateUrl: 'templates/home.html', 		controller: 'homeCtr' })
						.when('/about', 	{ templateUrl: 'templates/about.html', 		controller: 'aboutCtr' })
						.when('/contact', 	{ templateUrl: 'templates/contact.html', 	controller: 'contactCtr' })
	}])

	.controller('loginCtr', ['$scope', function($scope) {
		$scope.email = ""
		$scope.password = ""
	}])

	.controller('employeeCtr', ['$scope', function($scope) {
		$scope.discription = "About Controller says Hello ..!"
	}])

	.controller('taskCtr', ['$scope', function($scope) {
		$scope.discription = "Contact Controller says Hello ..!"
	}])