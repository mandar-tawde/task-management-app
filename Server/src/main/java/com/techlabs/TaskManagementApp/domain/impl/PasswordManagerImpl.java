package com.techlabs.TaskManagementApp.domain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.PasswordManager;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.exception.ValidationFailedException;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Password;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;
import com.techlabs.TaskManagementApp.repository.PasswordRepository;
import com.techlabs.TaskManagementApp.service.CryptoService;
import com.techlabs.TaskManagementApp.service.NotificationService;
import com.techlabs.TaskManagementApp.service.ObjectProcessorService;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;
import com.techlabs.TaskManagementApp.service.RandomGenerationService;

@Component
public class PasswordManagerImpl implements PasswordManager {

	@Autowired
	PasswordRepository passwordRepository;

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	ObjectValidationService objectValidationService;

	@Autowired
	ObjectProcessorService objectProcessorService;

	@Autowired
	RandomGenerationService randomGenerationService;

	@Autowired
	CryptoService cryptoService;

	@Autowired
	NotificationService notificationService;

	@Override
	public Password getPasswordOfEmployee(Long personId) {
		return passwordRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Password", "personId", personId));
	}

	@Override
	public Password setPassword(Long employeeId) throws Exception {

		Employee employee = employeeManager.getEmployeeById(employeeId);
		String email = employee.getPerson().getEmail();

		String passwordString = randomGenerationService.generatePassword(email);

		Password password = new Password();
		password.setPersonId(employeeId);
		password.setEmployee(employee);
		password.setPassword(cryptoService.encrypt(passwordString));
		password = passwordRepository.save(password);

		notificationService.sendNotificationNewPassword(employee.getPerson(), passwordString);
		return password;
	}

	@Override
	public Password changePassword(Long personId, ChangePasswordRequestBody requestBody, boolean isHashed)
			throws Exception {
		objectValidationService.validateChangePasswordRequest(requestBody);

		Password password = getPasswordOfEmployee(personId);
		String givenPassword = requestBody.getOldPassword();

		if (!isHashed)
			givenPassword = cryptoService.encrypt(givenPassword);

		if (!password.getPassword().equals(givenPassword))
			throw new ValidationFailedException("Password");

		password.setPassword(cryptoService.encrypt(requestBody.getNewPassword()));
		return passwordRepository.save(password);
	}

	@Override
	public void deletePassword(Long personId) {
		Password password = getPasswordOfEmployee(personId);
		passwordRepository.delete(password);
	}

	@Override
	public boolean doesPasswordExist(Long personId) {
		return passwordRepository.existsById(personId);
	}
}
