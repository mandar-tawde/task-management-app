package com.techlabs.TaskManagementApp.domain;

import java.util.List;

import com.techlabs.TaskManagementApp.model.Employee;

public interface EmployeeManager {

	public List<Employee> getAllEmployees();

	public Employee getEmployeeById(Long personId);

	public Employee createEmployee(Employee employee) throws Exception;

	public Employee updateEmployee(Long personId, Employee employeeDetails) throws Exception;

	public void deleteEmployee(Long personId);

	public boolean doesEmployeeExist(Long personId);
}
