package com.techlabs.TaskManagementApp.domain;

import java.util.List;

import com.techlabs.TaskManagementApp.model.Task;
import com.techlabs.TaskManagementApp.model.request.ChangeTaskStatusRequestBody;

public interface TaskManager {

	public List<Task> getAll();

	public Task getById(Long taskId);

	public List<Task> getByPersonId(Long personId);

	public List<Task> getByParentId(Long taskId);

	public Task create(Task task) throws Exception;

	public Task update(Long taskId, Task taskDetails) throws Exception;

	public Task changeStatus(Long taskId, ChangeTaskStatusRequestBody requestBody) throws Exception;

	public void delete(Long taskId);
}
