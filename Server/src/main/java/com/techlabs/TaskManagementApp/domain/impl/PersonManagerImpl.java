package com.techlabs.TaskManagementApp.domain.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.exception.ConflictException;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.repository.PersonRepository;
import com.techlabs.TaskManagementApp.service.ObjectProcessorService;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;

@Component
public class PersonManagerImpl implements PersonManager {

	@Autowired
	PersonRepository personRepository;

	@Autowired
	ObjectValidationService objectValidationService;

	@Autowired
	ObjectProcessorService objectProcessorService;

	@Override
	public List<Person> getAllPersons() {
		return personRepository.findAll();
	}

	@Override
	public Person getPersonById(Long personId) {
		return personRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));
	}

	@Override
	public Person getPersonByEmail(String email) {
		List<Person> persons = personRepository.findByEmail(email);
		if (persons.isEmpty())
			throw new ResourceNotFoundException("Person", "email", email);
		return persons.get(0);
	}

	@Override
	public Person addPerson(Person person) throws Exception {
		objectValidationService.validatePerson(person);
		try {
			return personRepository.save(person);
		} catch (DataIntegrityViolationException e) {
			throw new ConflictException(e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public Person updatePerson(Long personId, Person personDetails) throws Exception {
		Person person = getPersonById(personId);
		person = objectProcessorService.mergeObjects(personDetails, person);
		objectValidationService.validatePerson(person);

		try {
			return personRepository.save(person);
		} catch (DataIntegrityViolationException e) {
			throw new ConflictException(e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public void deletePerson(Long personId) {
		Person person = getPersonById(personId);
		objectValidationService.checkDependencyOfPerson(personId);
		personRepository.delete(person);
	}
}
