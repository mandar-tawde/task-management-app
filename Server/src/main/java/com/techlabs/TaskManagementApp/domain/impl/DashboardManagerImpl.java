package com.techlabs.TaskManagementApp.domain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.DashboardManager;
import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.OTPManager;
import com.techlabs.TaskManagementApp.domain.PasswordManager;
import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.exception.PreconditionsFailedException;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.exception.ValidationFailedException;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.OTP;
import com.techlabs.TaskManagementApp.model.Password;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;
import com.techlabs.TaskManagementApp.model.request.ResetPasswordWithOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.SendOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.VerifyOTPRequestBody;
import com.techlabs.TaskManagementApp.service.CryptoService;
import com.techlabs.TaskManagementApp.service.NotificationService;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;
import com.techlabs.TaskManagementApp.service.RandomGenerationService;

@Component
public class DashboardManagerImpl implements DashboardManager {

	@Autowired
	PersonManager personManager;

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	PasswordManager passwordManager;

	@Autowired
	OTPManager OTPManager;

	@Autowired
	ObjectValidationService objectValidationService;

	@Autowired
	RandomGenerationService randomGenerationService;

	@Autowired
	CryptoService cryptoService;

	@Autowired
	NotificationService notificationService;

	@Override
	public void sendOTP(SendOTPRequestBody requestBody) throws Exception {
		objectValidationService.validateSendOTPRequest(requestBody);

		Person person = personManager.getPersonByEmail(requestBody.getEmail());
		Employee employee = employeeManager.getEmployeeById(person.getId());
		String OTP_String = randomGenerationService.generateOTP();

		OTP OTP = new OTP();
		OTP.setPersonId(person.getId());
		OTP.setEmployee(employee);
		OTP.setOTP(cryptoService.encrypt(OTP_String));
		OTP.setVerified(false);

		OTPManager.addOTP(OTP);
		notificationService.sendNotificationOTP(person, OTP_String);
	}

	@Override
	public void verifyOTP(VerifyOTPRequestBody requestBody) throws Exception {
		objectValidationService.validateVerifyOTPRequest(requestBody);

		requestBody.setOTP(cryptoService.encrypt(requestBody.getOTP()));

		Person person = personManager.getPersonByEmail(requestBody.getEmail());
		OTP OTP = OTPManager.getOTPofEmployee(person.getId());

		if (!OTP.getOTP().equals(requestBody.getOTP()))
			throw new ValidationFailedException("OTP");

		OTP.setVerified(true);
		OTPManager.addOTP(OTP);
	}

	@Override
	public void resetPasswordWithOTP(ResetPasswordWithOTPRequestBody requestBody) throws Exception {
		objectValidationService.validateResetPasswordWithOTPRequest(requestBody);

		requestBody.setOTP(cryptoService.encrypt(requestBody.getOTP()));

		Person person = personManager.getPersonByEmail(requestBody.getEmail());
		Long personId = person.getId();
		OTP OTP = OTPManager.getOTPofEmployee(personId);

		if (!OTP.isVerified())
			throw new PreconditionsFailedException("Verify OTP");

		if (!OTP.getOTP().equals(requestBody.getOTP()))
			throw new ValidationFailedException("OTP");

		try {
			Password password = passwordManager.getPasswordOfEmployee(personId);

			ChangePasswordRequestBody changePasswordRequestBody = new ChangePasswordRequestBody();
			changePasswordRequestBody.setOldPassword(password.getPassword());
			changePasswordRequestBody.setNewPassword(requestBody.getNewPassword());

			passwordManager.changePassword(personId, changePasswordRequestBody, true);
			OTPManager.deleteOTPofEmployee(personId);

		} catch (ResourceNotFoundException e) {
			throw new Exception("Password already deleted");
		}
	}
}
