package com.techlabs.TaskManagementApp.domain;

import com.techlabs.TaskManagementApp.model.OTP;

public interface OTPManager {

	public OTP getOTPofEmployee(Long personId);

	public OTP addOTP(OTP OTP);

	public void deleteOTPofEmployee(Long personId);

	public boolean doesOTPExist(Long personId);
}
