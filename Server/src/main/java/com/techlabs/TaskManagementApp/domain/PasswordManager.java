package com.techlabs.TaskManagementApp.domain;

import com.techlabs.TaskManagementApp.model.Password;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;

public interface PasswordManager {

	public Password getPasswordOfEmployee(Long personId);

	public Password setPassword(Long personId) throws Exception;

	public Password changePassword(Long personId, ChangePasswordRequestBody requestBody, boolean isHashed)
			throws Exception;

	public void deletePassword(Long personId);

	public boolean doesPasswordExist(Long personId);
}
