package com.techlabs.TaskManagementApp.domain.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.exception.ConflictException;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.repository.EmployeeRepository;
import com.techlabs.TaskManagementApp.service.ObjectProcessorService;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;

@Component
public class EmployeeManagerImpl implements EmployeeManager {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	PersonManager personManager;

	@Autowired
	ObjectValidationService objectValidationService;

	@Autowired
	ObjectProcessorService objectProcessorService;

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee getEmployeeById(Long personId) {
		return employeeRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee", "personId", personId));
	}

	@Override
	public Employee createEmployee(Employee employee) throws Exception {
		objectValidationService.validateEmployee(employee);
		Long personId = employee.getPersonId();

		if (doesEmployeeExist(personId))
			throw new ConflictException("personId", personId.toString());

		Person person = personManager.getPersonById(personId);
		employee.setPerson(person);

		try {
			return employeeRepository.save(employee);
		} catch (DataIntegrityViolationException e) {
			throw new ConflictException(e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public Employee updateEmployee(Long personId, Employee employeeDetails) throws Exception {
		employeeDetails.setPersonId(null);
		Employee employee = getEmployeeById(personId);
		employee = objectProcessorService.mergeObjects(employeeDetails, employee);
		objectValidationService.validateEmployee(employee);

		try {
			return employeeRepository.save(employee);
		} catch (DataIntegrityViolationException e) {
			throw new ConflictException(e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public void deleteEmployee(Long personId) {
		Employee employee = getEmployeeById(personId);
		objectValidationService.checkDependencyOfEmployee(personId);
		employeeRepository.delete(employee);
	}

	@Override
	public boolean doesEmployeeExist(Long personId) {
		return employeeRepository.existsById(personId);
	}

}
