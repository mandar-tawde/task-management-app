package com.techlabs.TaskManagementApp.domain;

import com.techlabs.TaskManagementApp.model.request.ResetPasswordWithOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.SendOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.VerifyOTPRequestBody;

public interface DashboardManager {

	public void sendOTP(SendOTPRequestBody sendOTPRequestBody) throws Exception;

	public void verifyOTP(VerifyOTPRequestBody verifyOTPRequestBody) throws Exception;

	public void resetPasswordWithOTP(ResetPasswordWithOTPRequestBody requestBody) throws Exception;
}
