package com.techlabs.TaskManagementApp.domain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.OTPManager;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.model.OTP;
import com.techlabs.TaskManagementApp.repository.OTPRepository;

@Component
public class OTPManagerImpl implements OTPManager {

	@Autowired
	OTPRepository OTPRepository;

	@Override
	public OTP getOTPofEmployee(Long personId) {
		return OTPRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("OTP", "personId", personId));
	}

	@Override
	public OTP addOTP(OTP OTP) {
		return OTPRepository.save(OTP);
	}

	public void deleteOTPofEmployee(Long personId) {
		OTP OTP = getOTPofEmployee(personId);
		OTPRepository.delete(OTP);
	}

	@Override
	public boolean doesOTPExist(Long personId) {
		return OTPRepository.existsById(personId);
	}

}
