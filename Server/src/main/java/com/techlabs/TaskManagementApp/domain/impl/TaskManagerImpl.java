package com.techlabs.TaskManagementApp.domain.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.TaskManager;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Task;
import com.techlabs.TaskManagementApp.model.TaskStatus;
import com.techlabs.TaskManagementApp.model.request.ChangeTaskStatusRequestBody;
import com.techlabs.TaskManagementApp.repository.TaskRepository;
import com.techlabs.TaskManagementApp.service.NotificationService;
import com.techlabs.TaskManagementApp.service.ObjectProcessorService;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;

@Component
public class TaskManagerImpl implements TaskManager {

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	ObjectValidationService objectValidationService;

	@Autowired
	ObjectProcessorService objectProcessorService;

	@Autowired
	NotificationService notificationService;

	@Override
	public List<Task> getAll() {
		return taskRepository.findAll();
	}

	@Override
	public Task getById(Long taskId) {
		return taskRepository.findById(taskId).orElseThrow(() -> new ResourceNotFoundException("Task", "id", taskId));
	}

	@Override
	public List<Task> getByPersonId(Long personId) {
		Employee employee = employeeManager.getEmployeeById(personId);
		return taskRepository.findByAssignedTo(employee);
	}

	@Override
	public List<Task> getByParentId(Long taskId) {
		Task parent = getById(taskId);
		return taskRepository.findByParent(parent);
	}

	@Override
	public Task create(Task task) throws Exception {
		objectValidationService.validateTask(task);
		task.setStatus(TaskStatus.ASSIGNED);
		task = taskRepository.save(task);

		Employee assignedTo = task.getAssignedTo();
		if (assignedTo != null)
			notificationService.sendNotificationNewTask(assignedTo.getPerson(), task.getTitle());
		return task;
	}

	@Override
	public Task update(Long taskId, Task taskDetails) throws Exception {
		taskDetails.setId(null);

		Task task = getById(taskId);
		Employee wasAssignedTo = task.getAssignedTo();
		taskDetails.setStatus(task.getStatus());

		task = objectProcessorService.mergeObjects(taskDetails, task);

		if (taskDetails.getAssignedTo() != null)
			task.setParent(null);

		objectValidationService.validateTask(task);

		task = taskRepository.save(task);
		Employee assignedTo = task.getAssignedTo();

		if (wasAssignedTo == null) {
			if (assignedTo != null)
				notificationService.sendNotificationNewTask(assignedTo.getPerson(), task.getTitle());

		} else if (assignedTo != null) {
			if (assignedTo.getPersonId() != wasAssignedTo.getPersonId())
				notificationService.sendNotificationNewTask(assignedTo.getPerson(), task.getTitle());
		}

		return task;
	}

	@Override
	public Task changeStatus(Long taskId, ChangeTaskStatusRequestBody requestBody) throws Exception {
		objectValidationService.validateChangeTaskStatusRequest(requestBody);
		
		Task task = getById(taskId);
		task.setStatus(requestBody.getStatus());
		return taskRepository.save(task);
	}

	@Override
	public void delete(Long taskId) {
		Task task = getById(taskId);
		objectValidationService.checkDependencyOfTask(taskId);
		taskRepository.delete(task);
	}
}
