package com.techlabs.TaskManagementApp.domain;

import java.util.List;

import com.techlabs.TaskManagementApp.model.Person;

public interface PersonManager {

	public List<Person> getAllPersons();

	public Person getPersonById(Long personId);

	public Person getPersonByEmail(String email);

	public Person addPerson(Person person) throws Exception;

	public Person updatePerson(Long personId, Person personDetails) throws Exception;

	public void deletePerson(Long personId);
}
