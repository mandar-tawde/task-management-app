package com.techlabs.TaskManagementApp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.oauth2.service.ContextHelperService;

@RestController
@RequestMapping("/persons")
public class PersonController {

	@Autowired
	PersonManager personManager;

	@Autowired
	ContextHelperService contextHelperService;

	// Get All Persons
	@GetMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public List<Person> getAllPersons() {
		return personManager.getAllPersons();
	}

	// Get a Single Person
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Person getPersonById(@PathVariable(value = "id") Long personId) {
		return personManager.getPersonById(personId);
	}

	// Get a Single Person by logged in user
	@GetMapping("/self")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Person getPersonByToken() throws Exception {
		Long personId = contextHelperService.getPersonId();
		return personManager.getPersonById(personId);
	}

	// Create a new Person
	@PostMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Person createPerson(@Valid @RequestBody Person person) throws Exception {
		return personManager.addPerson(person);
	}

	// Update a Person
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Person updatePerson(@PathVariable(value = "id") Long personId, @Valid @RequestBody Person personDetails)
			throws Exception {
		return personManager.updatePerson(personId, personDetails);
	}

	// Update a Person by logged in user
	@PutMapping("/self")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Person updatePersonByToken(@Valid @RequestBody Person personDetails) throws Exception {
		Long personId = contextHelperService.getPersonId();
		return personManager.updatePerson(personId, personDetails);
	}

	// Delete a Person
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<?> deletePerson(@PathVariable(value = "id") Long personId) {
		personManager.deletePerson(personId);
		return ResponseEntity.ok().build();
	}
}
