package com.techlabs.TaskManagementApp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techlabs.TaskManagementApp.domain.DashboardManager;
import com.techlabs.TaskManagementApp.model.request.ResetPasswordWithOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.SendOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.VerifyOTPRequestBody;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	DashboardManager dashboardManager;

	// Request OTP
	@PostMapping("OTP")
	public ResponseEntity<?> sendOTP(@Valid @RequestBody SendOTPRequestBody sendOTPRequestBody) throws Exception {
		dashboardManager.sendOTP(sendOTPRequestBody);
		return ResponseEntity.ok().build();
	}

	// Verify OTP
	@PostMapping("OTP/Verify")
	public ResponseEntity<?> verifyOTP(@Valid @RequestBody VerifyOTPRequestBody verifyOTPRequestBody) throws Exception {

		dashboardManager.verifyOTP(verifyOTPRequestBody);
		return ResponseEntity.ok().build();
	}

	// Reset password using OTP
	@PostMapping("OTP/resetPassword")
	public ResponseEntity<?> resetPasswordWithOTP(@Valid @RequestBody ResetPasswordWithOTPRequestBody requestBody)
			throws Exception {

		dashboardManager.resetPasswordWithOTP(requestBody);
		return ResponseEntity.ok().build();
	}
}
