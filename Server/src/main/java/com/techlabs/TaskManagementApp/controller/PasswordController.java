package com.techlabs.TaskManagementApp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techlabs.TaskManagementApp.domain.PasswordManager;
import com.techlabs.TaskManagementApp.model.Password;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;
import com.techlabs.TaskManagementApp.oauth2.service.ContextHelperService;

@RestController
@RequestMapping("/passwords")
public class PasswordController {

	@Autowired
	PasswordManager passwordManager;

	@Autowired
	ContextHelperService contextHelperService;

	// Set password for employee
	@PostMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Password setPassword(@PathVariable(value = "id") Long personId) throws Exception {
		return passwordManager.setPassword(personId);
	}

	// Change password by logged in user
	@PutMapping("")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Password changePassword(@Valid @RequestBody ChangePasswordRequestBody changePasswordRequestBody)
			throws Exception {
		Long personId = contextHelperService.getPersonId();
		return passwordManager.changePassword(personId, changePasswordRequestBody, false);
	}

	// Delete password for employee
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<?> resetPassword(@PathVariable(value = "id") Long personId) {
		passwordManager.deletePassword(personId);
		return ResponseEntity.ok().build();
	}
}
