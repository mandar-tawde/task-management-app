package com.techlabs.TaskManagementApp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.oauth2.service.ContextHelperService;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	ContextHelperService contextHelperService;

	// Get All Employees
	@GetMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public List<Employee> getAllEmployees() {
		return employeeManager.getAllEmployees();
	}

	// Get an Single Employee
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Employee getEmployeeById(@PathVariable(value = "id") Long employeeId) {
		return employeeManager.getEmployeeById(employeeId);
	}

	// Get a Single Employee by logged in user
	@GetMapping("/self")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Employee getEmployeeByToken() throws Exception {
		Long employeeId = contextHelperService.getPersonId();
		return employeeManager.getEmployeeById(employeeId);
	}

	// Create an new Employee
	@PostMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Employee createEmployee(@Valid @RequestBody Employee employee) throws Exception {
		return employeeManager.createEmployee(employee);
	}

	// Update an Employee
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Employee updateEmployee(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody Employee employeeDetails) throws Exception {
		return employeeManager.updateEmployee(employeeId, employeeDetails);
	}

	// Update an Employee by logged in user
	@PutMapping("/self")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Employee updateEmployeeByToken(@Valid @RequestBody Employee employeeDetails) throws Exception {
		Long employeeId = contextHelperService.getPersonId();
		return employeeManager.updateEmployee(employeeId, employeeDetails);
	}

	// Delete an Employee
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long employeeId) {
		employeeManager.deleteEmployee(employeeId);
		return ResponseEntity.ok().build();
	}
}