package com.techlabs.TaskManagementApp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techlabs.TaskManagementApp.domain.TaskManager;
import com.techlabs.TaskManagementApp.model.Task;
import com.techlabs.TaskManagementApp.model.request.ChangeTaskStatusRequestBody;
import com.techlabs.TaskManagementApp.oauth2.service.ContextHelperService;

@RestController
@RequestMapping("/tasks")
public class TaskController {

	@Autowired
	TaskManager taskManager;

	@Autowired
	ContextHelperService contextHelperService;

	// Get All Tasks
	@GetMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public List<Task> getAllTasks() {
		return taskManager.getAll();
	}

	// Get a Single Task
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Task getTaskById(@PathVariable(value = "id") Long taskId) throws Exception {
		contextHelperService.verifyTaskOwnerOrAdmin(taskId);
		return taskManager.getById(taskId);
	}

	// Get tasks assigned to employee
	@GetMapping("/assignedTo/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public List<Task> getTaskAssignedTo(@PathVariable(value = "id") Long personId) {
		return taskManager.getByPersonId(personId);
	}

	// Get tasks assigned to logged in user
	@GetMapping("/assignedTo/self")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public List<Task> getTaskAssignedToLoggedinUser() throws Exception {
		Long personId = contextHelperService.getPersonId();
		return taskManager.getByPersonId(personId);
	}

	// Get sub-tasks of a task
	@GetMapping("/parent/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public List<Task> getSubTaskOf(@PathVariable(value = "id") Long taskId) throws Exception {
		contextHelperService.verifyTaskOwnerOrAdmin(taskId);
		return taskManager.getByParentId(taskId);
	}

	// Create a new Task
	@PostMapping("")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Task createTask(@Valid @RequestBody Task task) throws Exception {
		return taskManager.create(task);
	}

	// Update a Task
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Task updateTask(@PathVariable(value = "id") Long taskId, @Valid @RequestBody Task taskDetails)
			throws Exception {
		return taskManager.update(taskId, taskDetails);
	}

	// Change status of a Task
	@PutMapping("/status/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public Task changeStatusOfTask(@PathVariable(value = "id") Long taskId,
			@Valid @RequestBody ChangeTaskStatusRequestBody requestBody) throws Exception {
		contextHelperService.verifyTaskOwnerOrAdmin(taskId);
		return taskManager.changeStatus(taskId, requestBody);
	}

	// Delete a Task
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<?> deleteTask(@PathVariable(value = "id") Long taskId) {
		taskManager.delete(taskId);
		return ResponseEntity.ok().build();
	}
}
