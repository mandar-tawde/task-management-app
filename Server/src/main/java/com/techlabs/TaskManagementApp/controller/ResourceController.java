package com.techlabs.TaskManagementApp.controller;

import java.util.UUID;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

	@GetMapping("/context")
	@PreAuthorize("hasAuthority('ROLE_USER')")
	public String context() {
		return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	@GetMapping("/resource")
	public String resource() {
		return UUID.randomUUID().toString();
	}
}
