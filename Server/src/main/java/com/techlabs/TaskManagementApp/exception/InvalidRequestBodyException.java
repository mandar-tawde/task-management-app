package com.techlabs.TaskManagementApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRequestBodyException extends RuntimeException {

	private static final long serialVersionUID = 4141921761241535918L;

	public InvalidRequestBodyException(String message) {
		super(message);
	}
}
