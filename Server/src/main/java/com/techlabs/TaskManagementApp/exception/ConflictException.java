package com.techlabs.TaskManagementApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

	private static final long serialVersionUID = 6752988751385373636L;

	private String fieldName;
	private String value;

	public ConflictException(String message) {
		super(message);
	}

	public ConflictException(String fieldName, String value) {
		super(String.format("Duplicate %s :- %s", fieldName, value));
		this.fieldName = fieldName;
		this.value = value;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getValue() {
		return value;
	}
}
