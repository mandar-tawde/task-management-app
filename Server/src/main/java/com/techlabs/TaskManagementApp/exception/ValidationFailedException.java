package com.techlabs.TaskManagementApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class ValidationFailedException extends RuntimeException {

	private static final long serialVersionUID = -691957391207231143L;

	private String fieldName;

	public ValidationFailedException(String fieldName) {
		super(fieldName + " does not match");
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return fieldName;
	}
}
