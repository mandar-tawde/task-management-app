package com.techlabs.TaskManagementApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class PreconditionsFailedException extends RuntimeException {

	private static final long serialVersionUID = 2875390364118671245L;

	private String preCondition;

	public PreconditionsFailedException(String preCondition) {
		super(preCondition + " was not performed successfully");
		this.preCondition = preCondition;
	}

	public String getPreCondition() {
		return preCondition;
	}

}
