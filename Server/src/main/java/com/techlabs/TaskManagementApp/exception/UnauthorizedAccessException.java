package com.techlabs.TaskManagementApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedAccessException extends RuntimeException {

	private static final long serialVersionUID = -4500277081737423928L;

	private String resourceName;

	public UnauthorizedAccessException(String resourceName) {
		super("Unauthorized access to " + resourceName);
		this.resourceName = resourceName;
	}

	public String getResourceName() {
		return resourceName;
	}

}
