package com.techlabs.TaskManagementApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techlabs.TaskManagementApp.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
