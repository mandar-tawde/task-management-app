package com.techlabs.TaskManagementApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techlabs.TaskManagementApp.model.OTP;

public interface OTPRepository extends JpaRepository<OTP, Long> {

}
