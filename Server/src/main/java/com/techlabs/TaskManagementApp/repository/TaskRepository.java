package com.techlabs.TaskManagementApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {

	List<Task> findByAssignedTo(Employee assignedto);
	
	List<Task> findByParent(Task parent);
}
