package com.techlabs.TaskManagementApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techlabs.TaskManagementApp.model.Password;

public interface PasswordRepository extends JpaRepository<Password, Long> {

}
