package com.techlabs.TaskManagementApp.oauth2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.techlabs.TaskManagementApp.service.CryptoService;

public class CryptoEncoder implements PasswordEncoder {

	@Autowired
	CryptoService cryptoService;

	@Override
	public String encode(CharSequence rawPassword) {
		return cryptoService.encrypt(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return (encodedPassword.equals(encode(rawPassword)));
	}

}
