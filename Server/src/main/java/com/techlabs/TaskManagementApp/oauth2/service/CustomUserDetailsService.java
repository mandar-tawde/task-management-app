package com.techlabs.TaskManagementApp.oauth2.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.PasswordManager;
import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Password;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.oauth2.entity.Permission;
import com.techlabs.TaskManagementApp.oauth2.entity.Role;
import com.techlabs.TaskManagementApp.oauth2.entity.User;

@Service(value = "userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	PersonManager personManager;

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	PasswordManager passwordManager;

	private static Role adminRole;
	private static Role userRole;

	static {
		adminRole = new Role();
		adminRole.setName("ROLE_ADMIN");
		List<Permission> adminPermissions = new ArrayList<Permission>();
		adminPermissions.add(Permission.CAN_CREATE_EMPLOYEE);
		adminPermissions.add(Permission.CAN_DELETE_EMPLOYEE);
		adminRole.setPermissions(adminPermissions);

		userRole = new Role();
		userRole.setName("ROLE_USER");
		List<Permission> userPermissions = new ArrayList<Permission>();
		userPermissions.add(Permission.CAN_CHANGE_PASSWORD);
		userRole.setPermissions(userPermissions);
	}

	@Override
	public UserDetails loadUserByUsername(String username) {

		Person person = personManager.getPersonByEmail(username);
		Long personId = person.getId();
		Employee employee = employeeManager.getEmployeeById(personId);
		Password password = passwordManager.getPasswordOfEmployee(personId);

		User user = new User();
		user.setUsername(username);
		user.setPassword(password.getPassword());
		user.setPersonId(personId);

		List<Role> roles = new ArrayList<Role>();
		roles.add(userRole);

		if (employee.getIsAdmin())
			roles.add(adminRole);

		user.setRoles(roles);
		return user;
	}

}
