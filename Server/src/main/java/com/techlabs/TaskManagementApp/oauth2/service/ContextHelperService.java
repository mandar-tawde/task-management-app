package com.techlabs.TaskManagementApp.oauth2.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.domain.PersonManager;
import com.techlabs.TaskManagementApp.domain.TaskManager;
import com.techlabs.TaskManagementApp.exception.ResourceNotFoundException;
import com.techlabs.TaskManagementApp.exception.UnauthorizedAccessException;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.model.Task;

@Service
public class ContextHelperService {

	@Autowired
	PersonManager personManager;

	@Autowired
	TaskManager taskManager;

	public long getPersonId() throws Exception {
		String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Person person = null;

		try {
			person = personManager.getPersonByEmail(email);
		} catch (ResourceNotFoundException e) {
			throw new Exception("Person not found for email in context");
		}

		return person.getId();
	}

	public void verifyTaskOwnerOrAdmin(Long taskId) throws Exception {

		if (!isUserAdmin()) {
			Long loggedInPersonId = getPersonId();
			Task task = taskManager.getById(taskId);
			Task parentTask = task.getParent();

			if (parentTask != null)
				task = parentTask;

			Long ownerId = task.getAssignedTo().getPersonId();

			if (ownerId != loggedInPersonId)
				throw new UnauthorizedAccessException("Task : " + taskId);
		}
	}

	private boolean isUserAdmin() {
		Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();

		for (GrantedAuthority authority : authorities) {
			if ("ROLE_ADMIN".equals(authority.getAuthority()))
				return true;
		}
		return false;
	}
}
