package com.techlabs.TaskManagementApp.oauth2.entity;

public enum Permission {

	CAN_CREATE_EMPLOYEE,
	CAN_DELETE_EMPLOYEE,
	CAN_CHANGE_PASSWORD
}
