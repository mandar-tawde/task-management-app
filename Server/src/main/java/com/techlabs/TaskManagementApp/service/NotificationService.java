package com.techlabs.TaskManagementApp.service;

import com.techlabs.TaskManagementApp.model.Person;

public interface NotificationService {

	public void sendNotification(String messageTitle, String messageContent, String destination);

	public void sendNotificationNewPassword(Person person, String passwordString);

	public void sendNotificationNewTask(Person person, String taskTitle);

	public void sendNotificationOTP(Person person, String OTP_String);
}
