package com.techlabs.TaskManagementApp.service.impl;

import java.lang.reflect.Field;

import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.service.ObjectProcessorService;

@Service
public class ObjectProcessorServiceImpl implements ObjectProcessorService {

	@Override
	@SuppressWarnings("unchecked")
	public <O> O mergeObjects(O newObject, O oldObject) throws InstantiationException, IllegalAccessException {
		Class<?> clazz = newObject.getClass();
		Field[] fields = clazz.getDeclaredFields();
		Object mergedObject = clazz.newInstance();

		for (Field field : fields) {
			if (field.getName().equals("serialVersionUID"))
				continue;

			field.setAccessible(true);
			Object value1 = field.get(newObject);
			Object value2 = field.get(oldObject);
			Object value = (value1 != null) ? value1 : value2;
			field.set(mergedObject, value);
		}
		return (O) mergedObject;
	}

}
