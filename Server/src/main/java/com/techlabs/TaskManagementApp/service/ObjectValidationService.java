package com.techlabs.TaskManagementApp.service;

import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.model.Task;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;
import com.techlabs.TaskManagementApp.model.request.ChangeTaskStatusRequestBody;
import com.techlabs.TaskManagementApp.model.request.ResetPasswordWithOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.SendOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.VerifyOTPRequestBody;

public interface ObjectValidationService {

	public void validatePerson(Person person) throws Exception;

	public void validateEmployee(Employee employee) throws Exception;

	public void validateTask(Task task) throws Exception;

	public void validateChangePasswordRequest(ChangePasswordRequestBody requestBody) throws Exception;

	public void validateChangeTaskStatusRequest(ChangeTaskStatusRequestBody requestBody) throws Exception;

	public void validateResetPasswordWithOTPRequest(ResetPasswordWithOTPRequestBody requestBody) throws Exception;

	public void validateSendOTPRequest(SendOTPRequestBody requestBody) throws Exception;

	public void validateVerifyOTPRequest(VerifyOTPRequestBody requestBody) throws Exception;

	public void checkDependencyOfPerson(Long personId);

	public void checkDependencyOfEmployee(Long personId);

	public void checkDependencyOfTask(Long taskId);
}
