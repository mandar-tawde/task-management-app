package com.techlabs.TaskManagementApp.service;

public interface CryptoService {

	public String encrypt(String plainText);

	public String decrypt(String cipherText);
}
