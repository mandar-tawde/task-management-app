package com.techlabs.TaskManagementApp.service;

public interface ObjectProcessorService {

	public <O> O mergeObjects(O newObject, O oldObject) throws Exception;
}
