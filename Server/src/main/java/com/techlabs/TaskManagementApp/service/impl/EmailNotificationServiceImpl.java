package com.techlabs.TaskManagementApp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.repository.PersonRepository;
import com.techlabs.TaskManagementApp.service.NotificationService;

@Service
public class EmailNotificationServiceImpl implements NotificationService {

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	PersonRepository personRepository;

	@Value("${spring.mail.username}")
	private String username;

	@Override
	public void sendNotification(String messageTitle, String messageContent, String destination) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject(messageTitle);
		message.setText(messageContent);
		message.setTo(destination);
		message.setFrom(username);
		emailSender.send(message);
	}

	@Override
	public void sendNotificationNewPassword(Person person, String passwordString) {
		String email = person.getEmail();

		String subject = "New Credentials";
		String message = "Dear " + person.getName();
		message = message + "\nTo login in to Task-Managment-App, use following credentials\n";
		message = message + "Username\t:- " + email;
		message = message + "\nPassword\t:- " + passwordString;

		sendNotification(subject, message, email);
	}

	@Override
	public void sendNotificationNewTask(Person person, String taskTitle) {
		String email = person.getEmail();

		String subject = "New Task assigned";
		String message = "Dear " + person.getName();
		message = message + "\nNew task \"" + taskTitle + "\" is assigned to you.\n";
		sendNotification(subject, message, email);
	}

	@Override
	public void sendNotificationOTP(Person person, String OTP_String) {
		String email = person.getEmail();

		String subject = "OTP generated";
		String message = "Dear " + person.getName();
		message = message + "\nTo reset your password use \"" + OTP_String + "\" as an OTP.\n";
		sendNotification(subject, message, email);
	}

}
