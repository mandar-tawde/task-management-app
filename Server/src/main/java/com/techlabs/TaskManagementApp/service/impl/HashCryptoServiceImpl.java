package com.techlabs.TaskManagementApp.service.impl;

import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.service.CryptoService;

@Service
public class HashCryptoServiceImpl implements CryptoService {

	@Override
	public String encrypt(String plainText) {
		String cipherText = null;

		try {
			cipherText = byteArrayToHexString(computeHash(plainText));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Cipher algorithim not found");
		}

		return cipherText;
	}

	@Override
	public String decrypt(String cipherText) {
		// Can't decrypt once hashed
		return null;
	}

	private byte[] computeHash(String x) throws NoSuchAlgorithmException {
		java.security.MessageDigest d = null;
		d = java.security.MessageDigest.getInstance("SHA-1");
		d.reset();
		d.update(x.getBytes());
		return d.digest();
	}

	private String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

}
