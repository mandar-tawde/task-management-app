package com.techlabs.TaskManagementApp.service;

public interface RandomGenerationService {

	public String generateOTP();

	public String generatePassword(String email);
}
