package com.techlabs.TaskManagementApp.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.domain.EmployeeManager;
import com.techlabs.TaskManagementApp.domain.OTPManager;
import com.techlabs.TaskManagementApp.domain.PasswordManager;
import com.techlabs.TaskManagementApp.domain.TaskManager;
import com.techlabs.TaskManagementApp.exception.ConflictException;
import com.techlabs.TaskManagementApp.exception.InvalidRequestBodyException;
import com.techlabs.TaskManagementApp.model.Employee;
import com.techlabs.TaskManagementApp.model.Person;
import com.techlabs.TaskManagementApp.model.Task;
import com.techlabs.TaskManagementApp.model.request.ChangePasswordRequestBody;
import com.techlabs.TaskManagementApp.model.request.ChangeTaskStatusRequestBody;
import com.techlabs.TaskManagementApp.model.request.ResetPasswordWithOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.SendOTPRequestBody;
import com.techlabs.TaskManagementApp.model.request.VerifyOTPRequestBody;
import com.techlabs.TaskManagementApp.service.ObjectValidationService;

@Service
public class ObjectValidationServiceImpl implements ObjectValidationService {

	@Autowired
	EmployeeManager employeeManager;

	@Autowired
	PasswordManager passwordManager;

	@Autowired
	TaskManager taskManager;

	@Autowired
	OTPManager OTPManager;

	@SuppressWarnings("unchecked")
	@Override
	public void validatePerson(Person person) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("name");
		mandatoryFields.add("gender");
		mandatoryFields.add("address");
		mandatoryFields.add("contact");
		mandatoryFields.add("email");
		validateMandate(person, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		if (!checkOnlyCharacter(person.getName()))
			throw new InvalidRequestBodyException("Invalid name");

		if (!checkContact(person.getContact()))
			throw new InvalidRequestBodyException("Invalid contact");

		if (!checkEmail(person.getEmail()))
			throw new InvalidRequestBodyException("Invalid email");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateEmployee(Employee employee) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("personId");
		mandatoryFields.add("designation");
		mandatoryFields.add("isAdmin");
		validateMandate(employee, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		if (!checkOnlyCharacter(employee.getDesignation()))
			throw new InvalidRequestBodyException("Invalid designation");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateTask(Task task) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("title");
		validateMandate(task, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		Task parentTask = task.getParent();
		Employee assignedTo = task.getAssignedTo();

		if (assignedTo == null && parentTask == null)
			throw new InvalidRequestBodyException("Either assignedTo or parent must be given");

		if (parentTask != null) {
			Long parentId = parentTask.getId();

			List<Task> subTasks = taskManager.getByParentId(parentId);
			if (!subTasks.isEmpty())
				throw new ConflictException("This task has sub-tasks, cant be used as a sub-task");

			if (parentId == null)
				throw new InvalidRequestBodyException("In Parent 'id' is empty");

			parentTask = taskManager.getById(parentId);
			if (parentTask.getParent() != null)
				throw new ConflictException("Sub-task can not be used as a parent task");
			task.setParent(parentTask);
			task.setAssignedTo(null);

		} else {
			Long personId = assignedTo.getPersonId();

			if (personId == null)
				throw new InvalidRequestBodyException("In AssignedTo 'personId' is empty");

			assignedTo = employeeManager.getEmployeeById(personId);
			task.setAssignedTo(assignedTo);
			task.setParent(null);
		}

		if (task.getTitle().equals(""))
			throw new InvalidRequestBodyException("Invalid title");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateChangePasswordRequest(ChangePasswordRequestBody requestBody) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("oldPassword");
		mandatoryFields.add("newPassword");
		validateMandate(requestBody, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateChangeTaskStatusRequest(ChangeTaskStatusRequestBody requestBody) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("status");
		validateMandate(requestBody, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateResetPasswordWithOTPRequest(ResetPasswordWithOTPRequestBody requestBody) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("email");
		mandatoryFields.add("OTP");
		mandatoryFields.add("newPassword");
		validateMandate(requestBody, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		if (!checkEmail(requestBody.getEmail()))
			throw new InvalidRequestBodyException("Invalid email");

		if (!checkOTP(requestBody.getOTP()))
			throw new InvalidRequestBodyException("Invalid OTP");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateSendOTPRequest(SendOTPRequestBody requestBody) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("email");
		validateMandate(requestBody, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		if (!checkEmail(requestBody.getEmail()))
			throw new InvalidRequestBodyException("Invalid email");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateVerifyOTPRequest(VerifyOTPRequestBody requestBody) throws Exception {
		Map<String, Object> validationResult = new HashMap<String, Object>();

		List<String> mandatoryFields = new ArrayList<String>();
		mandatoryFields.add("email");
		mandatoryFields.add("OTP");
		validateMandate(requestBody, mandatoryFields, validationResult);

		if (!(boolean) validationResult.get("result")) {
			String message = generateMissingFieldsMessage((List<String>) validationResult.get("fields"));
			throw new InvalidRequestBodyException(message);
		}

		if (!checkEmail(requestBody.getEmail()))
			throw new InvalidRequestBodyException("Invalid email");

		if (!checkOTP(requestBody.getOTP()))
			throw new InvalidRequestBodyException("Invalid OTP");
	}

	@Override
	public void checkDependencyOfPerson(Long personId) {
		List<String> dependentObjects = new ArrayList<String>();

		if (employeeManager.doesEmployeeExist(personId)) {
			dependentObjects.add("Employee");

			List<Task> tasks = taskManager.getByPersonId(personId);
			if (!tasks.isEmpty())
				dependentObjects.add("Task");
		}

		if (passwordManager.doesPasswordExist(personId))
			dependentObjects.add("Password");

		if (OTPManager.doesOTPExist(personId))
			dependentObjects.add("OTP");

		if (!dependentObjects.isEmpty()) {
			String message = generateDependentObjectsMessage(dependentObjects);
			throw new ConflictException(message);
		}
	}

	@Override
	public void checkDependencyOfEmployee(Long personId) {
		List<String> dependentObjects = new ArrayList<String>();

		List<Task> tasks = taskManager.getByPersonId(personId);
		if (!tasks.isEmpty())
			dependentObjects.add("Task");

		if (passwordManager.doesPasswordExist(personId))
			dependentObjects.add("Password");

		if (OTPManager.doesOTPExist(personId))
			dependentObjects.add("OTP");

		if (!dependentObjects.isEmpty()) {
			String message = generateDependentObjectsMessage(dependentObjects);
			throw new ConflictException(message);
		}
	}

	@Override
	public void checkDependencyOfTask(Long taskId) {
		List<String> dependentObjects = new ArrayList<String>();

		List<Task> tasks = taskManager.getByParentId(taskId);
		if (!tasks.isEmpty())
			dependentObjects.add("Task");

		if (!dependentObjects.isEmpty()) {
			String message = generateDependentObjectsMessage(dependentObjects);
			throw new ConflictException(message);
		}
	}

	private void validateMandate(Object object, List<String> mandatoryFields, Map<String, Object> validationResult)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		List<String> missingFields = new ArrayList<String>();
		Boolean result = true;

		Class<?> clazz = object.getClass();

		for (String mandatoryField : mandatoryFields) {

			Field field = clazz.getDeclaredField(mandatoryField);
			field.setAccessible(true);

			Object value = field.get(object);
			if (value == null) {
				result = false;
				missingFields.add(mandatoryField);
			}
		}

		validationResult.put("result", result);
		validationResult.put("fields", missingFields);
	}

	private boolean checkOnlyCharacter(String string) {
		string = string.trim();
		return string.matches("[a-zA-Z ]+");
	}

	private boolean checkContact(String string) {
		return string.matches("(0/91)?[7-9][0-9]{9}");
	}

	private boolean checkEmail(String string) {
		return string.matches(
				"^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$");
	}

	private boolean checkOTP(String string) {
		return (string.matches("[0-9]+") && string.length() == 6);
	}

	private String generateMissingFieldsMessage(List<String> fields) {
		String message = "Fields [ ";
		for (String field : fields)
			message = message + field + ", ";
		message = message.substring(0, message.length() - 2) + " ] are empty";

		return message;
	}

	private String generateDependentObjectsMessage(List<String> dependentObjects) {
		String message = "Objects [ ";
		for (String dependentObject : dependentObjects)
			message = message + dependentObject + ", ";
		message = message.substring(0, message.length() - 2) + " ] are dependent on current object";

		return message;
	}
}
