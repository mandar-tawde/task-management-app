package com.techlabs.TaskManagementApp.service.impl;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.techlabs.TaskManagementApp.service.RandomGenerationService;

@Service
public class RandomGenerationServiceImpl implements RandomGenerationService {

	@Override
	public String generateOTP() {
		int randomNum = new Random().nextInt(999999 - 100000 + 1) + 100000;
		return String.valueOf(randomNum);
	}

	@Override
	public String generatePassword(String email) {
		String passwordString = email.split("@")[0];
		passwordString = passwordString + generateOTP();
		return passwordString;
	}

}
