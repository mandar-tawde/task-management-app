package com.techlabs.TaskManagementApp.model.request;

import java.io.Serializable;

public class ResetPasswordWithOTPRequestBody implements Serializable {

	private static final long serialVersionUID = -5939615939972285463L;

	private String email;
	private String OTP;
	private String newPassword;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String OTP) {
		this.OTP = OTP;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
