package com.techlabs.TaskManagementApp.model.request;

import com.techlabs.TaskManagementApp.model.TaskStatus;

public class ChangeTaskStatusRequestBody {

	private TaskStatus status;

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}
}
