package com.techlabs.TaskManagementApp.model;

public enum TaskStatus {

	ASSIGNED,
	ACCEPTED,
	REJECTED,
	WORKING,
	ON_HOLD,
	COMPLETED
}
