package com.techlabs.TaskManagementApp.model;

public enum Gender {
	MALE,
	FEMALE,
	TRANSGENDER
}
