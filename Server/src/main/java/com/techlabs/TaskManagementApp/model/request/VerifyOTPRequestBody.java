package com.techlabs.TaskManagementApp.model.request;

public class VerifyOTPRequestBody {

	private String email;
	private String OTP;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String OTP_String) {
		this.OTP = OTP_String;
	}
}
