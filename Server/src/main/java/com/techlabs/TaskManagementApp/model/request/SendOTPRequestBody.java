package com.techlabs.TaskManagementApp.model.request;

public class SendOTPRequestBody {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
